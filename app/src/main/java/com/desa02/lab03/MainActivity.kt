package com.desa02.lab03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {
            val nombres =  edtNombres.text.toString()
            val edad = edtEdad.text.toString()

            if(nombres.isEmpty()){
                Toast.makeText(this,"Ingresar Nombres", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(edad.isEmpty()){
                Toast.makeText(this,"Edad ingresar", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            var tipo = if(rbPerro.isChecked) "Perro" else if(rbGato.isChecked) "Gato" else "Conejo"
            var vacunas = ""
            vacunas += if (chkVac01.isChecked) getString(R.string.vac01) + "/" else ""
            vacunas += if(chkVac02.isChecked) getString(R.string.vac02) + "/" else ""
            vacunas += if(chkVac03.isChecked) getString(R.string.vac03) + "/" else ""
            vacunas += if(chkVac04.isChecked) getString(R.string.vac04) + "/" else ""
            vacunas += if(chkVac05.isChecked) getString(R.string.vac05) else ""

            val bundle = Bundle().apply {
                putString("key_nombres",nombres)
                putString("key_edad",edad)
                putString("key_tipo", tipo)
                putString("key_vacunas", vacunas)

            }

            val intent = Intent(this, SecondActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }
}
