package com.desa02.lab03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val bundleRecepcion: Bundle? = intent.extras

        bundleRecepcion?.let {
            val nombre = it.getString("key_nombres","") ?: ""
            val edad = it.getString("key_edad","") ?: ""
            val tipo =it.getString("key_tipo","") ?: ""

            val vacunas =it.getString("key_vacunas","") ?: ""

            tvResultadoNombres.text = nombre
            tvResultadoEdad.text = edad
            tvResultadoTipo.text = tipo
            tvResultadoVacunas.text = vacunas

            imvAnimal.setImageResource(
                when (tipo){
                    "Perro" ->{
                        R.drawable.ic_perro
                    }
                    "Gato" ->{
                        R.drawable.ic_gato
                    }
                    else -> R.drawable.ic_conejo
                }
            )


        }

    }
}
